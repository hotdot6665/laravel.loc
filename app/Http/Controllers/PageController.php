<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    public function About(){
        return view('pages.about');
    }

    public function Home(){

        return view('welcome');
    }
}